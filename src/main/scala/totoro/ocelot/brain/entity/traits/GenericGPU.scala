package totoro.ocelot.brain.entity.traits

import totoro.ocelot.brain.Settings
import totoro.ocelot.brain.entity.TextBuffer
import totoro.ocelot.brain.entity.machine.{Arguments, Callback, Context, Machine}
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.network.{Message, Network, Node, Visibility}
import totoro.ocelot.brain.util.{ColorDepth, PackedColor}
import totoro.ocelot.brain.workspace.Workspace

import scala.util.matching.Regex

// IMPORTANT: usually methods with side effects should *not* be direct
// callbacks to avoid the massive headache synchronizing them ensues, in
// particular when it comes to world saving. I'm making an exception for
// screens, though since they'd be painfully sluggish otherwise. This also
// means we have to use a somewhat nasty trick in Buffer's
// save function: we wait for all computers in the same network to finish
// their current execution and then pause them, to ensure the state of the
// buffer is "clean", meaning the computer has the correct state when it is
// saved in turn. If we didn't, a computer might change a screen after it was
// saved, but before the computer was saved, leading to mismatching states in
// the save file - a Bad Thing (TM).

trait GenericGPU extends Environment with Tiered {
  override val node: Node = Network.newNode(this, Visibility.Neighbors).
    withComponent("gpu").
    create()

  protected def maxResolution: (Int, Int) = Settings.screenResolutionsByTier(tier)

  protected def maxDepth: ColorDepth.Value = Settings.screenDepthsByTier(tier)

  private var screenAddress: Option[String] = None

  private var screenInstance: Option[TextBuffer] = None

  private def screen(f: TextBuffer => Array[AnyRef]): Array[AnyRef] = screenInstance match {
    case Some(screen) => screen.synchronized(f(screen))
    case _ => Array(null, "no screen")
  }

  final val setBackgroundCosts = Array(1.0 / 32, 1.0 / 64, 1.0 / 128)
  final val setForegroundCosts = Array(1.0 / 32, 1.0 / 64, 1.0 / 128)
  final val setPaletteColorCosts = Array(1.0 / 2, 1.0 / 8, 1.0 / 16)
  final val setCosts = Array(1.0 / 64, 1.0 / 128, 1.0 / 256)
  final val copyCosts = Array(1.0 / 16, 1.0 / 32, 1.0 / 64)
  final val fillCosts = Array(1.0 / 32, 1.0 / 64, 1.0 / 128)

  /**
   * Binds the GPU to some Screen node.
   * WARNING! Use with caution. This may disrupt the normal emulation flow.
   */
  def forceBind(node: Node, reset: Boolean = false): Unit = {
    node.host match {
      case buffer: TextBuffer =>
        screenAddress = Option(node.address)
        screenInstance = Some(buffer)
        screen(s => {
          if (reset) {
            val (gmw, gmh) = maxResolution
            val smw = s.getMaximumWidth
            val smh = s.getMaximumHeight
            s.setResolution(math.min(gmw, smw), math.min(gmh, smh))
            s.setColorDepth(ColorDepth(math.min(maxDepth.id, s.getMaximumColorDepth.id)))
            s.setForegroundColor(0xFFFFFF)
            s.setBackgroundColor(0x000000)
          }
          Array.empty
        })
      case _ =>
    }
  }

  def isBoundTo(address: String): Boolean = screenAddress.contains(address)

  def getScreenAddress: String = screenAddress.orNull

  // ----------------------------------------------------------------------- //

  @Callback(doc = """function(address:string[, reset:boolean=true]):boolean -- Binds the GPU to the screen with the specified address and resets screen settings if `reset` is true.""")
  def bind(context: Context, args: Arguments): Array[AnyRef] = {
    val address = args.checkString(0)
    val reset = args.optBoolean(1, default = true)
    node.network.node(address) match {
      case null => result((), "invalid address")
      case node: Node if node.host.isInstanceOf[TextBuffer] =>
        screenAddress = Option(address)
        screenInstance = Some(node.host.asInstanceOf[TextBuffer])
        screen(s => {
          if (reset) {
            val (gmw, gmh) = maxResolution
            val smw = s.getMaximumWidth
            val smh = s.getMaximumHeight
            s.setResolution(math.min(gmw, smw), math.min(gmh, smh))
            s.setColorDepth(ColorDepth(math.min(maxDepth.id, s.getMaximumColorDepth.id)))
            s.setForegroundColor(0xFFFFFF)
            s.setBackgroundColor(0x000000)
          }
          else context.pause(0) // To discourage outputting "in realtime" to multiple screens using one GPU.
          result(true)
        })
      case _ => result((), "not a screen")
    }
  }

  @Callback(direct = true, doc = """function():string -- Get the address of the screen the GPU is currently bound to.""")
  def getScreen(context: Context, args: Arguments): Array[AnyRef] = screen(s => result(s.node.address))

  @Callback(direct = true, doc = """function():number, boolean -- Get the current background color and whether it's from the palette or not.""")
  def getBackground(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(s.getBackgroundColor, s.isBackgroundFromPalette))

  @Callback(direct = true, doc = """function(value:number[, palette:boolean]):number, number or nil -- Sets the background color to the specified value. Optionally takes an explicit palette index. Returns the old value and if it was from the palette its palette index.""")
  def setBackground(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(setBackgroundCosts(tier))
    val color = args.checkInteger(0)
    screen(s => {
      val oldValue = s.getBackgroundColor
      val (oldColor, oldIndex) =
        if (s.isBackgroundFromPalette) {
          (s.getPaletteColor(oldValue), oldValue)
        }
        else {
          (oldValue, ())
        }
      s.setBackgroundColor(color, args.optBoolean(1, default = false))
      result(oldColor, oldIndex)
    })
  }

  @Callback(direct = true, doc = """function():number, boolean -- Get the current foreground color and whether it's from the palette or not.""")
  def getForeground(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(s.getForegroundColor, s.isForegroundFromPalette))

  @Callback(direct = true, doc = """function(value:number[, palette:boolean]):number, number or nil -- Sets the foreground color to the specified value. Optionally takes an explicit palette index. Returns the old value and if it was from the palette its palette index.""")
  def setForeground(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(setForegroundCosts(tier))
    val color = args.checkInteger(0)
    screen(s => {
      val oldValue = s.getForegroundColor
      val (oldColor, oldIndex) =
        if (s.isForegroundFromPalette) {
          (s.getPaletteColor(oldValue), oldValue)
        }
        else {
          (oldValue, ())
        }
      s.setForegroundColor(color, args.optBoolean(1, default = false))
      result(oldColor, oldIndex)
    })
  }

  @Callback(direct = true, doc = """function(index:number):number -- Get the palette color at the specified palette index.""")
  def getPaletteColor(context: Context, args: Arguments): Array[AnyRef] = {
    val index = args.checkInteger(0)
    screen(s => try result(s.getPaletteColor(index)) catch {
      case _: ArrayIndexOutOfBoundsException => throw new IllegalArgumentException("invalid palette index")
    })
  }

  @Callback(direct = true, doc = """function(index:number, color:number):number -- Set the palette color at the specified palette index. Returns the previous value.""")
  def setPaletteColor(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(setPaletteColorCosts(tier))
    val index = args.checkInteger(0)
    val color = args.checkInteger(1)
    context.pause(0.1)
    screen(s => try {
      val oldColor = s.getPaletteColor(index)
      s.setPaletteColor(index, color)
      result(oldColor)
    }
    catch {
      case _: ArrayIndexOutOfBoundsException => throw new IllegalArgumentException("invalid palette index")
    })
  }

  @Callback(direct = true, doc = """function():number -- Returns the currently set color depth.""")
  def getDepth(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(PackedColor.Depth.bits(s.getColorDepth)))

  @Callback(doc = """function(depth:number):number -- Set the color depth. Returns the previous value.""")
  def setDepth(context: Context, args: Arguments): Array[AnyRef] = {
    val depth = args.checkInteger(0)
    screen(s => {
      val oldDepth = s.getColorDepth
      depth match {
        case 1 => s.setColorDepth(ColorDepth.OneBit)
        case 4 if maxDepth.id >= ColorDepth.FourBit.id => s.setColorDepth(ColorDepth.FourBit)
        case 8 if maxDepth.id >= ColorDepth.EightBit.id => s.setColorDepth(ColorDepth.EightBit)
        case _ => throw new IllegalArgumentException("unsupported depth")
      }
      result(oldDepth)
    })
  }

  @Callback(direct = true, doc = """function():number -- Get the maximum supported color depth.""")
  def maxDepth(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(PackedColor.Depth.bits(ColorDepth(math.min(maxDepth.id, s.getMaximumColorDepth.id)))))

  @Callback(direct = true, doc = """function():number, number -- Get the current screen resolution.""")
  def getResolution(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(s.getWidth, s.getHeight))

  @Callback(doc = """function(width:number, height:number):boolean -- Set the screen resolution. Returns true if the resolution changed.""")
  def setResolution(context: Context, args: Arguments): Array[AnyRef] = {
    val w = args.checkInteger(0)
    val h = args.checkInteger(1)
    val (mw, mh) = maxResolution
    // Even though the buffer itself checks this again, we need this here for
    // the minimum of screen and GPU resolution.
    if (w < 1 || h < 1 || w > mw || h > mw || h * w > mw * mh)
      throw new IllegalArgumentException("unsupported resolution")
    screen(s => result(s.setResolution(w, h)))
  }

  @Callback(direct = true, doc = """function():number, number -- Get the maximum screen resolution.""")
  def maxResolution(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => {
      val (gmw, gmh) = maxResolution
      val smw = s.getMaximumWidth
      val smh = s.getMaximumHeight
      result(math.min(gmw, smw), math.min(gmh, smh))
    })

  @Callback(direct = true, doc = """function():number, number -- Get the current viewport resolution.""")
  def getViewport(context: Context, args: Arguments): Array[AnyRef] =
    screen(s => result(s.getViewportWidth, s.getViewportHeight))

  @Callback(doc = """function(width:number, height:number):boolean -- Set the viewport resolution. Cannot exceed the screen resolution. Returns true if the resolution changed.""")
  def setViewport(context: Context, args: Arguments): Array[AnyRef] = {
    val w = args.checkInteger(0)
    val h = args.checkInteger(1)
    val (mw, mh) = maxResolution
    // Even though the buffer itself checks this again, we need this here for
    // the minimum of screen and GPU resolution.
    if (w < 1 || h < 1 || w > mw || h > mw || h * w > mw * mh)
      throw new IllegalArgumentException("unsupported viewport size")
    screen(s => {
      if (w > s.getWidth || h > s.getHeight)
        throw new IllegalArgumentException("unsupported viewport size")
      result(s.setViewport(w, h))
    })
  }

  @Callback(direct = true, doc = """function(x:number, y:number):string, number, number, number or nil, number or nil -- Get the value displayed on the screen at the specified index, as well as the foreground and background color. If the foreground or background is from the palette, returns the palette indices as fourth and fifth results, else nil, respectively.""")
  def get(context: Context, args: Arguments): Array[AnyRef] = {
    val x = args.checkInteger(0) - 1
    val y = args.checkInteger(1) - 1
    screen(s => {
      val fgValue = s.getForegroundColor(x, y)
      val (fgColor, fgIndex) =
        if (s.isForegroundFromPalette(x, y)) {
          (s.getPaletteColor(fgValue), fgValue)
        }
        else {
          (fgValue, ())
        }

      val bgValue = s.getBackgroundColor(x, y)
      val (bgColor, bgIndex) =
        if (s.isBackgroundFromPalette(x, y)) {
          (s.getPaletteColor(bgValue), bgValue)
        }
        else {
          (bgValue, ())
        }

      result(s.get(x, y), fgColor, bgColor, fgIndex, bgIndex)
    })
  }

  @Callback(direct = true, doc = """function(x:number, y:number, value:string[, vertical:boolean]):boolean -- Plots a string value to the screen at the specified position. Optionally writes the string vertically.""")
  def set(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(setCosts(tier))
    val x = args.checkInteger(0) - 1
    val y = args.checkInteger(1) - 1
    val value = args.checkString(2)
    val vertical = args.optBoolean(3, default = false)

    screen(s => {
      s.set(x, y, value, vertical)
      result(true)
    })
  }

  @Callback(direct = true, doc = """function(x:number, y:number, width:number, height:number, tx:number, ty:number):boolean -- Copies a portion of the screen from the specified location with the specified size by the specified translation.""")
  def copy(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(copyCosts(tier))
    val x = args.checkInteger(0) - 1
    val y = args.checkInteger(1) - 1
    val w = math.max(0, args.checkInteger(2))
    val h = math.max(0, args.checkInteger(3))
    val tx = args.checkInteger(4)
    val ty = args.checkInteger(5)
    screen(s => {
      s.copy(x, y, w, h, tx, ty)
      result(true)
    })
  }

  @Callback(direct = true, doc = """function(x:number, y:number, width:number, height:number, char:string):boolean -- Fills a portion of the screen at the specified position with the specified size with the specified character.""")
  def fill(context: Context, args: Arguments): Array[AnyRef] = {
    context.consumeCallBudget(fillCosts(tier))
    val x = args.checkInteger(0) - 1
    val y = args.checkInteger(1) - 1
    val w = math.max(0, args.checkInteger(2))
    val h = math.max(0, args.checkInteger(3))
    val value = args.checkString(4)
    if (value.length == 1) screen(s => {
      s.fill(x, y, w, h, value.charAt(0))
      result(true)
    })
    else throw new Exception("invalid fill value")
  }

  // ----------------------------------------------------------------------- //

  override def onMessage(message: Message) {
    super.onMessage(message)
    if (message.name == "computer.stopped" && node.isNeighborOf(message.source)) {
      screen(s => {
        val (gmw, gmh) = maxResolution
        val smw = s.getMaximumWidth
        val smh = s.getMaximumHeight
        s.setResolution(math.min(gmw, smw), math.min(gmh, smh))
        s.setColorDepth(ColorDepth(math.min(maxDepth.id, s.getMaximumColorDepth.id)))
        s.setForegroundColor(0xFFFFFF)
        val w = s.getWidth
        val h = s.getHeight
        message.source.host match {
          case machine: Machine if machine.lastError != null =>
            if (s.getColorDepth.id > ColorDepth.OneBit.id) s.setBackgroundColor(0x0000FF)
            else s.setBackgroundColor(0x000000)
            s.fill(0, 0, w, h, ' ')
            try {
              val wrapRegEx = s"(.{1,${math.max(1, w - 2)}})\\s".r
              val lines = wrapRegEx.replaceAllIn(machine.lastError.replace("\t", "  ") + "\n", m => Regex.quoteReplacement(m.group(1) + "\n")).lines.toArray
              val firstRow = ((h - lines.length) / 2) max 2

              val message = "Unrecoverable Error"
              s.set((w - message.length) / 2, firstRow - 2, message, vertical = false)

              val maxLineLength = lines.map(_.length).max
              val col = ((w - maxLineLength) / 2) max 0
              for ((line, idx) <- lines.zipWithIndex) {
                val row = firstRow + idx
                s.set(col, row, line, vertical = false)
              }
            }
            catch {
              case t: Throwable => t.printStackTrace()
            }
          case _ =>
            s.setBackgroundColor(0x000000)
            s.fill(0, 0, w, h, ' ')
        }
        null // For screen()
      })
    }
  }

  override def onConnect(node: Node): Unit = {
    super.onConnect(node)
    if (screenInstance.isEmpty && screenAddress.fold(false)(_ == node.address)) {
      node.host match {
        case buffer: TextBuffer =>
          screenInstance = Some(buffer)
        case _ => // Not the screen node we're looking for.
      }
    }
  }

  override def onDisconnect(node: Node) {
    super.onDisconnect(node)
    if (node == this.node || screenAddress.contains(node.address)) {
      screenInstance = None
    }
  }

  // ----------------------------------------------------------------------- //

  private final val ScreenTag = "screen"

  override def load(nbt: NBTTagCompound, workspace: Workspace) {
    super.load(nbt, workspace)

    if (nbt.hasKey(ScreenTag)) {
      nbt.getString(ScreenTag) match {
        case screen: String if !screen.isEmpty => screenAddress = Some(screen)
        case _ => screenAddress = None
      }
      screenInstance = None
    }
  }

  override def save(nbt: NBTTagCompound) {
    super.save(nbt)

    if (screenAddress.isDefined) {
      nbt.setString(ScreenTag, screenAddress.get)
    }
  }
}

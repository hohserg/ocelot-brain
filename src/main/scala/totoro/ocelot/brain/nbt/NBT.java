package totoro.ocelot.brain.nbt;

public class NBT {
    public static int TAG_END = 0;
    public static int TAG_BYTE = 1;
    public static int TAG_SHORT = 2;
    public static int TAG_INT = 3;
    public static int TAG_LONG = 4;
    public static int TAG_FLOAT = 5;
    public static int TAG_DOUBLE = 6;
    public static int TAG_BYTE_ARRAY = 7;
    public static int TAG_STRING = 8;
    public static int TAG_LIST = 9;
    public static int TAG_COMPOUND = 10;
    public static int TAG_INT_ARRAY = 11;
}
